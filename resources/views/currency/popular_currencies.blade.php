@extends ('layouts.app')

@section('content')
<!--Table-->
<table class="table">

    <!--Table head-->
    <thead class="blue-grey lighten-4">
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Price</th>
        <th>Image</th>
        <th>Daily Change Percent</th>
    </tr>
    </thead>
    <!--Table head-->

    <!--Table body-->
    <tbody>
    @foreach($popularCurrencies as $currency)
    <tr>
        <th scope="row">{{ $currency->getId() }}</th>
        <td>{{ $currency->getName() }}</td>
        <td>{{ $currency->getPrice() }}</td>
        <td>{{ $currency->getImageUrl() }}</td>
        <td>{{ $currency->getDailyChangePercent() }}</td>
    </tr>
    @endforeach
    </tbody>
    <!--Table body-->

</table>
<!--Table-->

@endsection