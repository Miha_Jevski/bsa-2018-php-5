<?php

namespace Tests\Task1;

use App\Services\Currency;
use App\Services\CurrencyRepositoryInterface;

class InMemoryCurrencyRepository implements CurrencyRepositoryInterface
{
    private $currencies;

    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * @return Currency[]
     */
    public function findAll(): array
    {
        return $this->currencies;
    }

    /**
     * @param $length
     * @return array
     */
    public function getMostPopular($length): array
    {
        usort($this->currencies, function($a, $b) {
            return $b->getPrice() <=> $a->getPrice();
        });
        return array_slice($this->currencies, 0, $length);
    }

    /**
     * @return mixed
     */
    public function getMostChanged()
    {
        usort($this->currencies, function($a, $b) {
            return $b->getDailyChangePercent() <=> $a->getDailyChangePercent();
        });
        return array_shift ($this->currencies);
    }
}