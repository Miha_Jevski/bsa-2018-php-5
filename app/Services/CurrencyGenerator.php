<?php

namespace App\Services;

class CurrencyGenerator
{
    public static function generate(): array
    {
        $currencies = [];
        for ($i = 1; $i <= 10; $i++) {
            $currencies[] = new Currency(
                $i,
                'coin' . $i,
                rand(100,10000),
                'url' . $i,
                rand(1,99)/100
            );
        }
        return $currencies;
    }
}