<?php

namespace App\Services;

class GetMostChangedCurrencyCommandHandler
{
    private $repository;

    /**
     * GetMostChangedCurrencyCommandHandler constructor.
     * @param $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    public function handle(): Currency
    {
        return $this->repository->getMostChanged();
    }
}