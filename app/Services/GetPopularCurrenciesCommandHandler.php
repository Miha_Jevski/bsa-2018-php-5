<?php

namespace App\Services;

class GetPopularCurrenciesCommandHandler
{
    const POPULAR_COUNT = 3;

    private $repository;

    /**
     * GetPopularCurrenciesCommandHandler constructor.
     * @param $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    public function handle(): array
    {
        return $this->repository->getMostPopular(self::POPULAR_COUNT);
    }
}