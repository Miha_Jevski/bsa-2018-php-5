<?php

namespace App\Http\Controllers\Api;

use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;
use App\Services\GetCurrenciesCommandHandler;
use App\Services\GetMostChangedCurrencyCommandHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    public function index()
    {
        $repository = app()->make(CurrencyRepositoryInterface::class);
        $currencies = (new GetCurrenciesCommandHandler($repository))->handle();
        $output = [];
        foreach ($currencies as $currency){
            $output[] = CurrencyPresenter::present($currency);
        }
        return response()->json($output);
    }

    public function unstable()
    {
        $repository = app()->make(CurrencyRepositoryInterface::class);
        $currency = (new GetMostChangedCurrencyCommandHandler($repository))->handle();
        return response()->json(CurrencyPresenter::present($currency));
    }
}