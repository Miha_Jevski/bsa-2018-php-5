<?php

namespace App\Http\Controllers;

use App\Services\CurrencyRepositoryInterface;
use App\Services\GetPopularCurrenciesCommandHandler;
use Illuminate\Http\Request;

class CurrenciesController extends Controller
{
    public function popular()
    {
        $repository = app()->make(CurrencyRepositoryInterface::class);
        $popularCurrencies = (new GetPopularCurrenciesCommandHandler($repository))->handle();
        return view('currency.popular_currencies', compact('popularCurrencies'));
    }
}
