<?php

namespace App\Repositories;

use App\Services\Currency;
use App\Services\CurrencyRepositoryInterface;

class CurrencyRepository implements CurrencyRepositoryInterface
{
    private $currencies;

    /**
     * @param Currency[]
     */
    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * @return Currency[]
     */
    public function findAll(): array
    {
        return $this->currencies;
    }

    /**
     * @param $length
     * @return array
     */
    public function getMostPopular($length): array
    {
        usort($this->currencies, function($a, $b) {
            return $b->getPrice() <=> $a->getPrice();
        });
        return array_slice($this->currencies, 0, $length);
    }

    /**
     * @return mixed
     */
    public function getMostChanged()
    {
        usort($this->currencies, function($a, $b) {
            return $b->getDailyChangePercent() <=> $a->getDailyChangePercent();
        });
        return array_shift ($this->currencies);
    }
}